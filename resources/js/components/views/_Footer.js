exports.Footer = {
	view: () => {
		return [
			m('#footer', [
				m('.footer-grabber', [
					m('span.icon.icon-resize-bottom-right')
				])
			])
		]
	}
}